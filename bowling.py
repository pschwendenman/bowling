'''
simple module to hold bowling game class
'''

class Game():
    '''
    represent the progress of the bowling game.
    '''
    def __init__(self):
        '''
        initialize the class
        '''
        self._rolls = []

    def roll(self, pins):
        '''
        save the roll for processing later
        '''
        self._rolls.append(pins)

    @classmethod
    def yield_frame(cls, rolls):
        '''
        group the game into logical frames for summation
        '''
        if rolls:
            # Strike
            if rolls[0] == 10:
                yield rolls[:3]
                rest = rolls[1:]
            # Spare
            elif not len(rolls) < 2 and rolls[0] + rolls[1] == 10:
                yield rolls[:3]
                rest = rolls[2:]
            # Open frame
            else:
                yield rolls[:2]
                rest = rolls[2:]
            yield from cls.yield_frame(rest)

    def frame_scores(self):
        '''
        return a list of scores for each frame
        '''
        return list(sum(frame) for frame in self.yield_frame(self._rolls))[:10]

    def score(self):
        '''
        return the current score for the game
        '''
        return sum(self.frame_scores())
