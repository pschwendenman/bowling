'''
tests for bowling module
'''
import unittest
import bowling


class TestBowlingGame(unittest.TestCase):
    '''
    test cases from kata (mostly)
    '''
    def setUp(self):
        pass

    def test_all_gutters(self):
        '''
        ensure score is accurate for all gutterballs
        '''
        game = bowling.Game()

        for _ in range(10):
            game.roll(0)

        self.assertEqual(game.score(), 0)

    def test_all_ones(self):
        '''
        ensure score is accurate for inly single pins
        '''
        game = bowling.Game()

        for _ in range(10):
            game.roll(1)

        self.assertEqual(game.score(), 10)

    def test_spare(self):
        '''
        ensure score is accurate for spare
        '''
        game = bowling.Game()

        self.roll_spare(game)
        game.roll(4)
        game.roll(1)

        self.assertEqual(game.score(), 19)

    def test_strike(self):
        '''
        ensure code is accurate for strike
        '''
        game = bowling.Game()

        self.roll_strike(game)
        game.roll(3)
        game.roll(4)

        self.assertEqual(game.score(), 24)

    def test_perfect_game(self):
        '''
        ensure score is accurate for perfect game
        '''
        game = bowling.Game()

        for _ in range(12):
            game.roll(10)

        self.assertEqual(game.score(), 300)

    @staticmethod
    def roll_spare(game):
        '''
        roll a spare
        '''
        game.roll(5)
        game.roll(5)

    @staticmethod
    def roll_strike(game):
        '''
        bowl a strike
        '''
        game.roll(10)


if __name__ == '__main__':
    unittest.main()
